const height = require('./tailwind.h_w.config')
const width = height


/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx,vue}"
  ],
  theme: {

    screens: {
      xs: '425px',
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1280px',
      tablet: '640px',
      laptop: '1024px',
      desktop: '1280px'
    },
    extend: {

      zIndex:{
        0: '0',
        2: '2',
        4: '4',
        6: '6',
      },
      minWidth: {
        ...height,
        0: '0',
        '1/4': '25%',
        'tr': '1.3px',

        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      maxWidth: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      minHeight: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      maxHeight: {
        ...height,
        0: '0',
        '1/4': '25%',
        '1/2': '50%',
        '1/3': '33.33333333%',
        '2/3': '66.66666666%',
        '3/4': '75%',
        full: '100%'
      },
      margin: {
        '17': '68px',
        '38':'152px',
        '54':'216px',
        '66':'264px'
      },
      padding: {
        '1/2': '0.125rem',
        '3/2': '0.375rem',
        '10p': '10px',
        '6/5': '1.375rem',
        '5/4': '1.125rem',
        ...height
      },
      fontFamily: {
        sans: 'Sf Pro',
        open:'Open Sans'

      },
      fontSize: {
        '6xl': ['41px', { lineHeight: '49px' }],
        '5xl': ['38px', { lineHeight: '25px' }],
        '4xl': ['32px', { lineHeight: '39px' }],
        '3xl': ['30px', { lineHeight: '36px' }],
        '2xl': ['24px', { lineHeight: '34px' }],
        '2.5xl': ['28px', { lineHeight: '25px' }],
        xl2: ['22px', { lineHeight: '28px' }],
        xl3: ['22px', { lineHeight: '25px' }],
        xl: ['20px', { lineHeight: '24px' }],
        lg3: ['18px', { lineHeight: '32px' }],
        lg2: ['18px', { lineHeight: '20px' }],
        lg1: ['16px', { lineHeight: '32px' }],
        lg: ['16px', { lineHeight: '21px' }],
        base: ['15px', { lineHeight: '18px' }],
        sm: ['14px', { lineHeight: '19px' }],
        sml: ['14px', { lineHeight: '25px' }],

        12: ['12px', { lineHeight: '25px' }],
        xs: ['11px', { lineHeight: '13px' }],
        10: ['10px', { lineHeight: '12px' }],
      },
      colors: {
        gr:'#707070 ',
        grl:'#717171',
        grll:'#D9D9D9',
        bl: '#C0D0E8',
        blu: '#0575DD',
        blue1:'#4A84DB',
        nude: '#e1ded8',
        warning: '#feb62b',
        success: '#149b19',
        primary: '#b91919',
        'primary-900': '#AC1313',
        ternary: '#83040E',
        akcent: '#8f0606',
        'dark-gray': '#393939',
        'light-gray': '#F7F7F7 ',
        // gray: '#8F8F8F',
        gray: {
          DBE0EC:'#DBE0EC',
          DEFAULT: '#8F8F8F',
          dark: '#393939',
          normal: '#F0F0F0',
          light: '#9a9a9a'
        },
        neutral: '#616161',
        white: '#ffffff',
        wh:'#F2F3F5',
        redl:'#d82422',
        whi:'#DFE4EF',
        black: '#000000',
        milk: '#CFCCC6',
        wax: '#2B2B2B',
        cherry: '#AC1313',
        mediator: '#E1DFDB',
        red:'#FF2C3D'

      },
      height,
      width,
      rotate: {
        26: '26deg',
        154: '154deg',
        330: '330deg',
        30: '30deg',
        206: '206deg'
      },
      borderRadius: {
        '3xl':'7px',
        '4xl': '13px',
        '5xl': '14px',
        '6xl': '20px'
      },
      boxShadow: {
        DEFAULT: '0 0 6px 0 rgba(0, 0, 0, 0.16)',
        card:'0 0 10px #12182219',
        profile:'0px 4px 15px #2169D534'
      },
      gridTemplateRows: {
        7: 'repeat(7, minmax(0, 1fr))',
        8: 'repeat(8, minmax(0, 1fr))',
        9: 'repeat(9, minmax(0, 1fr))',
        10: 'repeat(10, minmax(0, 1fr))',
        11: 'repeat(11, minmax(0, 1fr))',
        12: 'repeat(12, minmax(0, 1fr))',
        13: 'repeat(13, minmax(0, 1fr))',
        14: 'repeat(14, minmax(0, 1fr))',
        15: 'repeat(15, minmax(0, 1fr))',
        16: 'repeat(16, minmax(0, 1fr))',
        17: 'repeat(17, minmax(0, 1fr))',
        18: 'repeat(18, minmax(0, 1fr))',
        19: 'repeat(19, minmax(0, 1fr))',
        20: 'repeat(20, minmax(0, 1fr))'
      }
    }
  },
  plugins: [],
}
